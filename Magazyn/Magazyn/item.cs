﻿using System;
using System.Collections.Generic;

namespace Magazyn
{
    public class Item
    {
        public Guid Id;
        public int Amount;
        public int IntCathergory;
        public int PriceToBuy;
        public int PriceToSell;
        public string Name;
        public int ItemProfit;
        public int ItemCostB;
        public int ItemCostA;

        public Item( Guid id, int intcrathegory, int amount, int pricetobuy, int pricetosell,string name)
        {
            this.Id = id;
            this.IntCathergory = intcrathegory;
            this.PriceToBuy = pricetobuy;
            this.PriceToSell = pricetosell;
            this.Name = name;
            this.Amount = amount;
        }



    }
}