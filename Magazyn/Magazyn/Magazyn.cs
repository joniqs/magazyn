﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazyn
{
   public class Magazyn
    {
      public  List<Item> Items;

        public Magazyn()
        {
            Items = new List<Item>();
        }
        
        public void AddItem(int intcrathegory, int amount, int pricetobuy, int pricetosell, string name)
        {
            
            Items.Add(new Item(Guid.NewGuid(),intcrathegory, amount, pricetobuy, pricetosell, name));

        }
        
        public void ShowContent()
        {
            Console.WriteLine();
            Console.WriteLine("W sklepie jest :");
          
              foreach (var item in Items)
                {
                    Console.WriteLine(item.Amount + " " +item.Name);
                    
                }
            
        }

        public void SoldItems()
        {
            
            foreach (var item in Items)
            {
                item.ItemCostB = item.Amount * item.PriceToBuy;
                Console.WriteLine();
                Console.WriteLine("W sklepie jest: " + item.Amount + " " + item.Name);
                Console.WriteLine("Ile " + item.Name + " zostało sprzedanych");
                int a1= int.Parse(Console.ReadLine());
                int a2 = item.Amount;
                int a3 = a2 - a1;
                item.Amount = a3;
                item.ItemCostA = a1 * item.PriceToSell;
                item.ItemProfit = item.ItemCostA - item.ItemCostB;
                Console.WriteLine("Profit z " + item.Name + " = " + item.ItemProfit);
            }
            
        }

        public void SortItems()
        {
            Console.WriteLine("**********Sortowanie**********");
            Items = Items.OrderBy(x => x.Name).ToList();
        }



    }
}
