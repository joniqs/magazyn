﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazyn
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Magazyn item = new Magazyn();

            Console.WriteLine("Podaj ile chcesz dodać rzeczy");
            int a = int.Parse(Console.ReadLine());
            for (int i = 0; i < a ; i++)
            {

                Console.WriteLine("Podaj liczbę towaru, cenę kupienia i jego sprzedania, oraz jego nazwę");
                item.AddItem(1,int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), Console.ReadLine());
            }
            item.ShowContent();
            item.SoldItems();
            item.ShowContent();
            Console.WriteLine("Jeśli chcesz dodać nowy przedmiot wpisz 1, jeśli nie wpisz 0");
            int c = int.Parse(Console.ReadLine());
            if (c>0)
            {
                Console.WriteLine("Podaj liczbę towaru, cenę kupienia i jego sprzedania, oraz jego nazwę");
                item.AddItem(1, int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), Console.ReadLine());
            }
            
            item.ShowContent();
            item.SortItems();
            item.ShowContent();

            Console.ReadLine();
        }
    }
}
